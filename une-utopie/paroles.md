c'est un ultime paysage  
un magnifique voyage  
une cascade qui tombe  
sans bruit de feu ni de bombe  

végétation luxuriante  
la nuit devient notre amante  
des papillons de couleur  
qui embellissent les fleurs  

### Refrain 2x

| dansons sous les étoiles  
| créons notre propre toile  
| dans l'utopie de ce monde  
| où seules les féées sont fécondes  

une pleine lune blanche  
au pays de la féé franche  
tout n'est qu'amour et passion  
ou tout existe sauf la raison  

une petite main magique  
source d'eau bénéfique  
nénuphare de lotus  
c'est un magnifique consensus  

### Refrain 1x

des libellules musicales  
qui chantent d'un son royal  
une belle danse impériale  
qui de nos yeux nous régale  

saurons-nous cultiver ce jardin  
paradis au milieu des pins  
dans nos têtes corrompues  
de cet ensemble déjà vu  
