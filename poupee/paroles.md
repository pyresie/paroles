Petite poupée toute sanglante  
En apparence si transparente  
Renfermant un florilège de misère  
pour enfin enfouir son grand mystère  

#### Refrain

| son coeur revient de l'imaginaire  
| son monde enfantin devient son frère  
| elle s'enferme dans sa petite bulle  
| dans son parterre remplit de tulle  

Un sourire qui renferme des larmes  
un rire qui devient sa plus grande arme  
perdue dans sa lourde solitude  
son être redevient lassitude

### Refrain

### Solo

Mais comment évoluer dans ses songes  
pris au fond d'elle, ne sont que purs mensonges  
continuer de fermer ses petits yeux  
pour ne point éteindre son jeune feu  

### Solo

### Refrain
