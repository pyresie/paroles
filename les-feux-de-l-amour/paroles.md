une foule de fans scandant ce divin nom  
une génération qui ne fait qu'un seul bond  
tous ensemble idolant la même personne  
et qui s'insurge à croire que la terre est bonne  

ils coient à leur roue pour refaire le monde  
leur paroles de salle de concert innondent  
ils achètent tout les produits dérivés  
tout cela leur semble complètement sensé  

### Refrain 2x

| mais rien d'autre n'a d'importance  
| pour ces individus en trance  
| tous ensemble suivant leur roi  
| ainsi est le leur loi  

mais sont-ils conscients de ne plus rien penser  
et de par eux-mêmes ils n'ont plus d'autres idées  
ils se protègent dans un monde utopique  
alors ce décor n'est que factice  

suivant inconsciement un être comme nous  
ils ne se rendent pas compte qu'ils deviennent fous  
c'est la dictature du nouveau millénaire  
où toutes les pensées deviennent précaires   

### Refrain 2x
