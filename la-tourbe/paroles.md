je suis incapable de tolérer  
où je vis dans cette cage dorée  
les barreaux construits un à un  
m'empêchent d'apercevoir la fin  

j'ai pourtant façonné tout ces murs  
qui malgré tout me rassurent  
mais à trop vouloir être bien  
on ouvre les yeux sur les siens  

### Refrain

| j'aimais cette vie qui avance  
| mais j'ai oublié les conséquences  
| j'ai envie de tout faire sauter  
| pour accéder à la liberté  

je me réveille et j'aperçois  
que cette vie n'est pas pour moi  
trop de contraintes, trop de non-dits  
à quoi sert-il de rester ici  

des mots prononcés par habitude  
d'autres retenus par la quiétude  
plus de place pour la vérité  
je suis en train de m'éffacer  

### Refrain

### Intermède musical

ouvrir les yeux fait bien trop de mal  
car l'espoir me traverse comme une balle  
mais à attendre toujours plus  
on ouvre les yeux sur sa mue  
au risque de ne jamais m'en sortir  
je ne sais même pas ce qui est pire  

### Refrain
