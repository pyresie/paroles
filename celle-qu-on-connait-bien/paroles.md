le silence me travaille  
comme une plaie qui sur mon coeur se taille  
le mutisme qui ronge mon corps  
me ramène tout droit à mon sort  

je suis obligé de me cacher  
pour continuer à avancer  
chaque pas en avant me ramène  
dans les coulisse de ma scène  

### Refrain

| mais tu ne veux rien voir  
| toi tu préfères le noir  
| tu te plais dans ton utopie  
| y'a comme du bonheur dans ta vie  

à l'aube des temps où tous se noient  
je ne veux plus suivre la voie  
que l'extérieur me force à suivre  
tout m'édicter pour me faire vivre  

à ressasser c'qu'on à vécu  
à regretter c'qu'on à pas vu  
à trébucher contre l'idéal  
je sais que je m'fais trop de mal  

### Refrain

### Intermède musical

mais à quoi sert-il de s'obstiner  
à vivre une vie toute pensée  
je préfère foncer droit dans le mur  
dans un avenir pas vraiment sûr  

mieux veux céder à ses fantasmes  
que de mourrir dans le marrasme  
alors qu'est-ce qui me retient  
sinon la conscience qui m'appartient  

### Refrain 2x
