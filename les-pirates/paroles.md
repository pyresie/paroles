je vous présente les jeunes voyageurs  
qui ensemble n'ont jamais eu peur  
le borgne est un homme grand et craint de tous  
le petit nouveau est un jeune mousse  
le capitaine l'a ram'né après s'être saoulé  

le capitaine est un homme solitaire  
mais pourtant il est certainement père  
le tricorne lui se soumet aux ordres  
il est toujours gentil sans jamais mordre  
sans oublier le plus susceptible qu'il y ait à bord  

### refrain

| et guidés par le vent  
| pourtant résonne leur chant  
| leur rire est envoûtant  
| ainsi ils tuent le temps  

un matin comblé de grandes nouvelles eaux (comblé ?? pas sûr du mot)  
survint une pluie à vous tremper les os  
ils tentèrent même d'installer un abri  
mais la pluie transforma tout en débris  
ils n'urent pas le choix que de fuire cet affreux orage  

devant un verre de whisky et de rhum  
un des gars voulant faire son grand homme  
sorti de la poche une boussole  
en étant sûr qu'elle indiquait le grand pôle  
résultat : ils prirent tous une aut' direction  

### refrain

### intermède musical

arrivés dans un endroit inconnu  
ils virent que la boussole ne marchait plus  
mais au fond ce qui compte est d'être ensemble  
ainsi leur équipage jamais ne tremble  
les voila en route pour de nouvelles aventures  
